package it.intesa.salesreport.controller.inputbean;


public class SalesReportInputBean {

	private String partener_FE;
	private String partener_AS;
	private String prodotto;
	private String startDate;
	private String endDate;
	
	public String getPartener_FE() {
		return partener_FE;
	}
	public void setPartener_FE(String partener_FE) {
		this.partener_FE = partener_FE;
	}
	public String getPartener_AS() {
		return partener_AS;
	}
	public void setPartener_AS(String partener_AS) {
		this.partener_AS = partener_AS;
	}
	public String getProdotto() {
		return prodotto;
	}
	public void setProdotto(String prodotto) {
		this.prodotto = prodotto;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
}
