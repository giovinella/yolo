package it.intesa.salesreport.controller.outputbean;

public class SalesReport {

	private BaseRegistryData baseRegistryData;
	private BaseRegistryData insuranceData;
	private ProductData productData;
	private AdditionalData additionalData;
	
	
	public BaseRegistryData getBaseRegistryData() {
		return baseRegistryData;
	}
	public void setBaseRegistryData(BaseRegistryData baseRegistryData) {
		this.baseRegistryData = baseRegistryData;
	}
	public BaseRegistryData getInsuranceData() {
		return insuranceData;
	}
	public void setInsuranceData(BaseRegistryData insuranceData) {
		this.insuranceData = insuranceData;
	}
	public ProductData getProductData() {
		return productData;
	}
	public void setProductData(ProductData productData) {
		this.productData = productData;
	}
	public AdditionalData getAdditionalData() {
		return additionalData;
	}
	public void setAdditionalData(AdditionalData additionalData) {
		this.additionalData = additionalData;
	}
}
