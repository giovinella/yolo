package it.intesa.salesreport.controller.outputbean;

public class ContactData {

	private String email;
	private String number;
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
}
