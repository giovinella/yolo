package it.intesa.salesreport.controller.outputbean;

public class AdditionalData {

	private String policyNumber;
	private String clientCode;
	private String policySubscriptionDate;
	private String coverageStartDate;
	private String coverageEndDate;
	private String coverageDuration;
	private String insuredItems;
	private String premiumAmount;
	private String orderStatus;
	private String subscriptionChannel;
	public String getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	public String getClientCode() {
		return clientCode;
	}
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
	public String getPolicySubscriptionDate() {
		return policySubscriptionDate;
	}
	public void setPolicySubscriptionDate(String policySubscriptionDate) {
		this.policySubscriptionDate = policySubscriptionDate;
	}
	public String getCoverageStartDate() {
		return coverageStartDate;
	}
	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}
	public String getCoverageEndDate() {
		return coverageEndDate;
	}
	public void setCoverageEndDate(String coverageEndDate) {
		this.coverageEndDate = coverageEndDate;
	}
	public String getCoverageDuration() {
		return coverageDuration;
	}
	public void setCoverageDuration(String coverageDuration) {
		this.coverageDuration = coverageDuration;
	}
	public String getInsuredItems() {
		return insuredItems;
	}
	public void setInsuredItems(String insuredItems) {
		this.insuredItems = insuredItems;
	}
	public String getPremiumAmount() {
		return premiumAmount;
	}
	public void setPremiumAmount(String premiumAmount) {
		this.premiumAmount = premiumAmount;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getSubscriptionChannel() {
		return subscriptionChannel;
	}
	public void setSubscriptionChannel(String subscriptionChannel) {
		this.subscriptionChannel = subscriptionChannel;
	}
	
	
}
