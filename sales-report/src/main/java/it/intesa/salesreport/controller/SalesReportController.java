package it.intesa.salesreport.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.intesa.salesreport.controller.inputbean.SalesReportInputBean;
import it.intesa.salesreport.controller.outputbean.SalesReport;
import it.intesa.salesreport.controller.outputbean.common.BodySalesReport;
import it.intesa.salesreport.controller.outputbean.common.Result;
import it.intesa.salesreport.controller.outputbean.common.SalesReportOutputBean;
import it.intesa.salesreport.services.apiintesa.ApiIntesaService;
import it.intesa.salesreport.utility.Constants;

/**
 * 
 * @author Francesco Marchitelli
 * @Email f.marchitelli@be-tse.it
 * 
 */

@RestController
@RequestMapping("/intesa")
public class SalesReportController {

	@Autowired ApiIntesaService apiIntesaService;
	
	@GetMapping("/salesreport")
	public SalesReportOutputBean salesReport(@RequestBody SalesReportInputBean salesReportInputBean) {
		
		SalesReportOutputBean salesReportOutPutBean = new SalesReportOutputBean();
		BodySalesReport bodySalesReport = new BodySalesReport();
		Result result = new Result();
		
		try {
			List<SalesReport> list = apiIntesaService.getAllDataView(salesReportInputBean);
			if(list!=null&&!list.isEmpty()) {
				
				result.setId(Constants.SUCCESS);
				result.setMessage(Constants.OK);
				bodySalesReport.setListSalesReport(list);
				salesReportOutPutBean.setBody(bodySalesReport);
				salesReportOutPutBean.setResult(result);
				
			} else {
				
				result.setId(Constants.NOTFOUND);
				result.setMessage(Constants.NOTFOUNDMSG);
				salesReportOutPutBean.setBody(bodySalesReport);
				salesReportOutPutBean.setResult(result);
				
			}
		} catch (Exception e) {
			
			result.setId(Constants.EXCEPTION);
			result.setMessage(Constants.KO + e);
			salesReportOutPutBean.setResult(result);
		}
		
		return salesReportOutPutBean;
		
	}
	
	
}
