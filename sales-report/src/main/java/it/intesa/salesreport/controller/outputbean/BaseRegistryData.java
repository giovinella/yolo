package it.intesa.salesreport.controller.outputbean;

public class BaseRegistryData {

	private String name;
	private String surname;
	private BirthData birthData;
	private AddressData residence;
	private AddressData domicile;
	private ContactData contactData;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public BirthData getBirthData() {
		return birthData;
	}
	public void setBirthData(BirthData birthData) {
		this.birthData = birthData;
	}
	public ContactData getContactData() {
		return contactData;
	}
	public void setContactData(ContactData contactData) {
		this.contactData = contactData;
	}
	public AddressData getResidence() {
		return residence;
	}
	public void setResidence(AddressData residence) {
		this.residence = residence;
	}
	public AddressData getDomicile() {
		return domicile;
	}
	public void setDomicile(AddressData domicile) {
		this.domicile = domicile;
	}
}
