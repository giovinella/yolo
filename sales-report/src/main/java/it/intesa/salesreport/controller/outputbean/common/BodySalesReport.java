package it.intesa.salesreport.controller.outputbean.common;

import java.util.List;

import it.intesa.salesreport.controller.outputbean.SalesReport;

public class BodySalesReport {

	private List<SalesReport> listSalesReport;

	public List<SalesReport> getListSalesReport() {
		return listSalesReport;
	}

	public void setListSalesReport(List<SalesReport> listSalesReport) {
		this.listSalesReport = listSalesReport;
	}
}
