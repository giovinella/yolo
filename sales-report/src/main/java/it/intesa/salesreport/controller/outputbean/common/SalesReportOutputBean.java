package it.intesa.salesreport.controller.outputbean.common;


public class SalesReportOutputBean {

	private BodySalesReport body;
	private Result result;
	
	public BodySalesReport getBody() {
		return body;
	}
	public void setBody(BodySalesReport body) {
		this.body = body;
	}
	public Result getResult() {
		return result;
	}
	public void setResult(Result result) {
		this.result = result;
	}
}
