package it.intesa.salesreport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SalesReport {

	public static void main(String[] args) {
		SpringApplication.run(SalesReport.class, args);
	}

}
