package it.intesa.salesreport.repositories.apiintesa;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import it.intesa.salesreport.entities.apiintesa.ApiIntesa;

@Repository
public interface ApiIntesaRepository extends CrudRepository<ApiIntesa,Long> {


	public List<ApiIntesa> findByDataSottoscrizionePolizzaBetweenAndDescrizioneProdottoOrderByDataSottoscrizionePolizzaDesc (Date dataInizio,Date dataFine,String prodotto);
	
	public List<ApiIntesa> findByDataSottoscrizionePolizzaBetweenOrderByDataSottoscrizionePolizzaDesc (Date dataInizio,Date dataFine);
	
}
