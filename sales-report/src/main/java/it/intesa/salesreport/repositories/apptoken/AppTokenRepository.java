package it.intesa.salesreport.repositories.apptoken;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.intesa.salesreport.entities.apptoken.AppToken;

@Repository
public interface AppTokenRepository extends CrudRepository<AppToken,Long> {

}
