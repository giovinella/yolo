package it.intesa.salesreport.entities.apiintesa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema="be_admin",name="api_intesa")
public class ApiIntesa implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue()
	private Long id_view;
	@Column(name="nome")
	private String nome;
	@Column(name="cognome")
	private String cognome;
	@Column(name="localita_nascita")
	private String localitaNascita;
	@Column(name="provincia_nascita")
	private String provinciaNascita;
	@Column(name="nazione_nascita")
	private String nazioneNascita;
	@Column(name="data_di_nascita")
	private Date dataNascita;
	@Column(name="codice_fiscale")
	private String codiceFiscale;
	@Column(name="indirizzo_residenza")
	private String indirizzoResidenza;
	@Column(name="localita_residenza")
	private String localitaResidenza;
	@Column(name="provincia_residenza")
	private String provinciaResidenza;
	@Column(name="nazione_residenza")
	private String nazioneResidenza;
	@Column(name="indirizzo_domicilio")
	private String indirizzoDomicilio;
	@Column(name="provincia_domicilio")
	private String provinciaDomicilio;
	@Column(name="localita_domicilio")
	private String localitaDomicilio;
	@Column(name="nazione_domicilio")
	private String nazioneDomicilio;
	@Column(name="numero_di_telefono")
	private String numeroTelefono;
	@Column(name="email")
	private String email;
	@Column(name="nome_assicurato")
	private String nomeAssicurato;
	@Column(name="cognome_assicurato")
	private String cognomeAssicurato;
	@Column(name="localita_nascita_assicurato")
	private String localitaNascitaAssicurato;
	@Column(name="provincia_nascita_assicurato")
	private String provinciaNascitaAssicurato;
	@Column(name="nazione_nascita_assicurato")
	private String nazioneNascitaAssicurato;
	@Column(name="data_nascita_assicurato")
	private Date dataNascitaAssicurato;
	@Column(name="codice_fiscale_assicurato")
	private String codiceFiscaleAssicurato;
	@Column(name="microchip")
	private String microchip;
	@Column(name="indirizzo_residenza_assicurato")
	private String indirizzoResidenzaAssicurato;
	@Column(name="localita_residenza_assicurato")
	private String localitaResidenzaAssicurato;
	@Column(name="provincia_residenza_assicurato")
	private String provinciaResidenzaAssicurato;
	@Column(name="nazione_residenza_assicurato")
	private String nazioneResidenzaAssicurato;
	@Column(name="numero_di_telefono_assicurato")
	private String numeroTelefonoAssicurato;
	@Column(name="email_assicurato")
	private String emailAssicurato;
	@Column(name="codice_prodotto")
	private String codiceProdotto;
	@Column(name="descrizione_prodotto")
	private String descrizioneProdotto;
	@Column(name="tipologia_prodotto")
	private String tipologiaProdotto;
	@Column(name="numero_di_polizza")
	private String numeroPolizza;
	@Column(name="codice_cliente_yolo")
	private String codiceClienteYOLO;
	@Column(name="data_sottoscrizione_polizza")
	private Date dataSottoscrizionePolizza;
	@Column(name="data_inizio_copertura")
	private Date dataInizioCopertura;
	@Column(name="data_fine_copertura")
	private Date dataFineCopertura;
	@Column(name="durata")
	private Long durata;
	@Column(name="importo_premio")
	private Long importoPremio;
	@Column(name="canale_sottoscrizione")
	private String canaleSottoscrizione;
	@Column(name="stato_ordine")
	private String statoOrdine;
	
	
	public Long getId_view() {
		return id_view;
	}
	public void setId_view(Long id_view) {
		this.id_view = id_view;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getLocalitaNascita() {
		return localitaNascita;
	}
	public void setLocalitaNascita(String localitaNascita) {
		this.localitaNascita = localitaNascita;
	}
	public String getProvinciaNascita() {
		return provinciaNascita;
	}
	public void setProvinciaNascita(String provinciaNascita) {
		this.provinciaNascita = provinciaNascita;
	}
	public String getNazioneNascita() {
		return nazioneNascita;
	}
	public void setNazioneNascita(String nazioneNascita) {
		this.nazioneNascita = nazioneNascita;
	}
	public Date getDataNascita() {
		return dataNascita;
	}
	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}
	public String getCodiceFiscale() {
		return codiceFiscale;
	}
	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}
	public String getIndirizzoResidenza() {
		return indirizzoResidenza;
	}
	public void setIndirizzoResidenza(String indirizzoResidenza) {
		this.indirizzoResidenza = indirizzoResidenza;
	}
	public String getProvinciaResidenza() {
		return provinciaResidenza;
	}
	public void setProvinciaResidenza(String provinciaResidenza) {
		this.provinciaResidenza = provinciaResidenza;
	}
	public String getNazioneResidenza() {
		return nazioneResidenza;
	}
	public void setNazioneResidenza(String nazioneResidenza) {
		this.nazioneResidenza = nazioneResidenza;
	}
	public String getIndirizzoDomicilio() {
		return indirizzoDomicilio;
	}
	public void setIndirizzoDomicilio(String indirizzoDomicilio) {
		this.indirizzoDomicilio = indirizzoDomicilio;
	}
	public String getProvinciaDomicilio() {
		return provinciaDomicilio;
	}
	public void setProvinciaDomicilio(String provinciaDomicilio) {
		this.provinciaDomicilio = provinciaDomicilio;
	}
	public String getNazioneDomicilio() {
		return nazioneDomicilio;
	}
	public void setNazioneDomicilio(String nazioneDomicilio) {
		this.nazioneDomicilio = nazioneDomicilio;
	}
	public String getNumeroTelefono() {
		return numeroTelefono;
	}
	public void setNumeroTelefono(String numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNomeAssicurato() {
		return nomeAssicurato;
	}
	public void setNomeAssicurato(String nomeAssicurato) {
		this.nomeAssicurato = nomeAssicurato;
	}
	public String getCognomeAssicurato() {
		return cognomeAssicurato;
	}
	public void setCognomeAssicurato(String cognomeAssicurato) {
		this.cognomeAssicurato = cognomeAssicurato;
	}
	public String getLocalitaNascitaAssicurato() {
		return localitaNascitaAssicurato;
	}
	public void setLocalitaNascitaAssicurato(String localitaNascitaAssicurato) {
		this.localitaNascitaAssicurato = localitaNascitaAssicurato;
	}
	public String getProvinciaNascitaAssicurato() {
		return provinciaNascitaAssicurato;
	}
	public void setProvinciaNascitaAssicurato(String provinciaNascitaAssicurato) {
		this.provinciaNascitaAssicurato = provinciaNascitaAssicurato;
	}
	public String getNazioneNascitaAssicurato() {
		return nazioneNascitaAssicurato;
	}
	public void setNazioneNascitaAssicurato(String nazioneNascitaAssicurato) {
		this.nazioneNascitaAssicurato = nazioneNascitaAssicurato;
	}
	public Date getDataNascitaAssicurato() {
		return dataNascitaAssicurato;
	}
	public void setDataNascitaAssicurato(Date dataNascitaAssicurato) {
		this.dataNascitaAssicurato = dataNascitaAssicurato;
	}
	public String getCodiceFiscaleAssicurato() {
		return codiceFiscaleAssicurato;
	}
	public void setCodiceFiscaleAssicurato(String codiceFiscaleAssicurato) {
		this.codiceFiscaleAssicurato = codiceFiscaleAssicurato;
	}
	public String getMicrochip() {
		return microchip;
	}
	public void setMicrochip(String microchip) {
		this.microchip = microchip;
	}
	public String getIndirizzoResidenzaAssicurato() {
		return indirizzoResidenzaAssicurato;
	}
	public void setIndirizzoResidenzaAssicurato(String indirizzoResidenzaAssicurato) {
		this.indirizzoResidenzaAssicurato = indirizzoResidenzaAssicurato;
	}
	public String getLocalitaResidenzaAssicurato() {
		return localitaResidenzaAssicurato;
	}
	public void setLocalitaResidenzaAssicurato(String localitaResidenzaAssicurato) {
		this.localitaResidenzaAssicurato = localitaResidenzaAssicurato;
	}
	public String getProvinciaResidenzaAssicurato() {
		return provinciaResidenzaAssicurato;
	}
	public void setProvinciaResidenzaAssicurato(String provinciaResidenzaAssicurato) {
		this.provinciaResidenzaAssicurato = provinciaResidenzaAssicurato;
	}
	public String getNazioneResidenzaAssicurato() {
		return nazioneResidenzaAssicurato;
	}
	public void setNazioneResidenzaAssicurato(String nazioneResidenzaAssicurato) {
		this.nazioneResidenzaAssicurato = nazioneResidenzaAssicurato;
	}
	public String getNumeroTelefonoAssicurato() {
		return numeroTelefonoAssicurato;
	}
	public void setNumeroTelefonoAssicurato(String numeroTelefonoAssicurato) {
		this.numeroTelefonoAssicurato = numeroTelefonoAssicurato;
	}
	public String getEmailAssicurato() {
		return emailAssicurato;
	}
	public void setEmailAssicurato(String emailAssicurato) {
		this.emailAssicurato = emailAssicurato;
	}
	public String getCodiceProdotto() {
		return codiceProdotto;
	}
	public void setCodiceProdotto(String codiceProdotto) {
		this.codiceProdotto = codiceProdotto;
	}
	public String getDescrizioneProdotto() {
		return descrizioneProdotto;
	}
	public void setDescrizioneProdotto(String descrizioneProdotto) {
		this.descrizioneProdotto = descrizioneProdotto;
	}
	public String getTipologiaProdotto() {
		return tipologiaProdotto;
	}
	public void setTipologiaProdotto(String tipologiaProdotto) {
		this.tipologiaProdotto = tipologiaProdotto;
	}
	public String getNumeroPolizza() {
		return numeroPolizza;
	}
	public void setNumeroPolizza(String numeroPolizza) {
		this.numeroPolizza = numeroPolizza;
	}
	public String getCodiceClienteYOLO() {
		return codiceClienteYOLO;
	}
	public void setCodiceClienteYOLO(String codiceClienteYOLO) {
		this.codiceClienteYOLO = codiceClienteYOLO;
	}
	public Date getDataSottoscrizionePolizza() {
		return dataSottoscrizionePolizza;
	}
	public void setDataSottoscrizionePolizza(Date dataSottoscrizionePolizza) {
		this.dataSottoscrizionePolizza = dataSottoscrizionePolizza;
	}
	public Date getDataInizioCopertura() {
		return dataInizioCopertura;
	}
	public void setDataInizioCopertura(Date dataInizioCopertura) {
		this.dataInizioCopertura = dataInizioCopertura;
	}
	public Date getDataFineCopertura() {
		return dataFineCopertura;
	}
	public void setDataFineCopertura(Date dataFineCopertura) {
		this.dataFineCopertura = dataFineCopertura;
	}
	public Long getDurata() {
		return durata;
	}
	public void setDurata(Long durata) {
		this.durata = durata;
	}
	public Long getImportoPremio() {
		return importoPremio;
	}
	public void setImportoPremio(Long importoPremio) {
		this.importoPremio = importoPremio;
	}
	public String getCanaleSottoscrizione() {
		return canaleSottoscrizione;
	}
	public void setCanaleSottoscrizione(String canaleSottoscrizione) {
		this.canaleSottoscrizione = canaleSottoscrizione;
	}
	public String getStatoOrdine() {
		return statoOrdine;
	}
	public void setStatoOrdine(String statoOrdine) {
		this.statoOrdine = statoOrdine;
	}
	public String getLocalitaResidenza() {
		return localitaResidenza;
	}
	public void setLocalitaResidenza(String localitaResidenza) {
		this.localitaResidenza = localitaResidenza;
	}
	public String getLocalitaDomicilio() {
		return localitaDomicilio;
	}
	public void setLocalitaDomicilio(String localitaDomicilio) {
		this.localitaDomicilio = localitaDomicilio;
	}
}
