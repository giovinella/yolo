package it.intesa.salesreport.utility;

public class Constants {

	public static final String SUCCESS = "WS-000";
	public static final String NOTFOUND = "WS-001";
	public static final String EXCEPTION = "SYS-001";
	public static final String OK = "- OK";
	public static final String KO = "KO -";
	public static final String NOTFOUNDMSG = "- Result not found";
}
