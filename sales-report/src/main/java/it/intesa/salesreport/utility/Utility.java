package it.intesa.salesreport.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Utility {

	public static Date processingEndDate (Date date) {
		Calendar c =Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.HOUR,23);
		c.add(Calendar.MINUTE,59);
		c.add(Calendar.SECOND,59);
		Date result = c.getTime();
		return result;
	}
	
	
	public static Date processingStringEndDate (String dateEnd) throws ParseException {
		Date date = new SimpleDateFormat("yyyy-MM-dd").parse(dateEnd);
		Calendar c =Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.HOUR,23);
		c.add(Calendar.MINUTE,59);
		c.add(Calendar.SECOND,59);
		Date result = c.getTime();
		return result;
	}
	
	public static Date processingStringStartDate (String dateEnd) throws ParseException {
		Date date = new SimpleDateFormat("yyyy-MM-dd").parse(dateEnd);
		return date;
	}
}
