package it.intesa.salesreport.services.apiintesa;

import java.text.ParseException;
import java.util.List;

import it.intesa.salesreport.controller.inputbean.SalesReportInputBean;
import it.intesa.salesreport.controller.outputbean.SalesReport;

public interface ApiIntesaService {

	public List<SalesReport> getAllDataView(SalesReportInputBean inputBean) throws ParseException ;
	
}
