package it.intesa.salesreport.services.apiintesa;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.intesa.salesreport.controller.inputbean.SalesReportInputBean;
import it.intesa.salesreport.controller.outputbean.SalesReport;
import it.intesa.salesreport.entities.apiintesa.ApiIntesa;
import it.intesa.salesreport.repositories.apiintesa.ApiIntesaRepository;
import it.intesa.salesreport.utility.Utility;

@Service
public class ApiIntesaImp implements ApiIntesaService {

	@Autowired ApiIntesaRepository apiIntesaRepository;
	@Autowired ApiIntesaManager apiMng;
	@Override
	public List<SalesReport> getAllDataView(SalesReportInputBean inputBean) throws ParseException {
		List<ApiIntesa> listDb = new ArrayList<>();
		
		if(null!=inputBean.getProdotto() && !inputBean.getProdotto().equals("")){
			listDb=	apiIntesaRepository.findByDataSottoscrizionePolizzaBetweenAndDescrizioneProdottoOrderByDataSottoscrizionePolizzaDesc(Utility.processingStringStartDate(inputBean.getStartDate()), Utility.processingStringEndDate(inputBean.getEndDate()), inputBean.getProdotto());
		}else
			{
			listDb=	apiIntesaRepository.findByDataSottoscrizionePolizzaBetweenOrderByDataSottoscrizionePolizzaDesc(Utility.processingStringStartDate(inputBean.getStartDate()), Utility.processingStringEndDate(inputBean.getEndDate()));
			}	
		
		
		return apiMng.apiIntesatoSalesReportOutputBean(listDb);
	}

}
