package it.intesa.salesreport.services.apiintesa;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import it.intesa.salesreport.controller.outputbean.AdditionalData;
import it.intesa.salesreport.controller.outputbean.AddressData;
import it.intesa.salesreport.controller.outputbean.BaseRegistryData;
import it.intesa.salesreport.controller.outputbean.BirthData;
import it.intesa.salesreport.controller.outputbean.ContactData;
import it.intesa.salesreport.controller.outputbean.ProductData;
import it.intesa.salesreport.controller.outputbean.SalesReport;
import it.intesa.salesreport.entities.apiintesa.ApiIntesa;
@Component
public class ApiIntesaManager {

	String pattern = "yyyy-MM-dd";
    DateFormat df = new SimpleDateFormat(pattern);
	
public List<SalesReport> apiIntesatoSalesReportOutputBean (List<ApiIntesa> apiIntesaBeanLista){
	List<SalesReport> listOutput = new ArrayList<>();
	
	SalesReport output =null;
	for(ApiIntesa a: apiIntesaBeanLista){
		
		output = new SalesReport();
		
		
		output.setBaseRegistryData(apiIntesatoBaseRegistryData( a));
		output.setInsuranceData(apiIntesatoBaseRegistryDataAssicurato( a));
		output.setProductData(apiIntesatoProductData( a));
		output.setAdditionalData(apiIntesatoAdditionalData( a));
		
		listOutput.add(output);
		
	}
	
	 
	return listOutput;
}	
	
public BaseRegistryData apiIntesatoBaseRegistryData (ApiIntesa apiIntesaBean){
	
	 BaseRegistryData baseRegistryData = new BaseRegistryData();
	 
	 if(null!=apiIntesaBean.getNome()){
		 baseRegistryData.setName(apiIntesaBean.getNome());
	 }
	 if(null!=apiIntesaBean.getCognome()){
		 baseRegistryData.setSurname(apiIntesaBean.getCognome());	
	 }
	 

	BirthData birthData= new BirthData();
	if(null!=apiIntesaBean.getDataNascita()){
		birthData.setBirthDate(df.format(apiIntesaBean.getDataNascita()));
	 }
	if(null!=apiIntesaBean.getCodiceFiscale()){
		birthData.setFiscalCode(apiIntesaBean.getCodiceFiscale());
	 }
	
	if(null!=apiIntesaBean.getLocalitaNascita()){
		birthData.setLocality(apiIntesaBean.getLocalitaNascita());
	 }
	
	if(null!=apiIntesaBean.getNazioneNascita()){
		birthData.setNation(apiIntesaBean.getNazioneNascita());
	 }
	
	if(null!=apiIntesaBean.getProvinciaNascita()){
		birthData.setProvince(apiIntesaBean.getProvinciaNascita());
	 }
	
	baseRegistryData.setBirthData(birthData);
	
	baseRegistryData.setResidence(apiIntesatoAddressData(apiIntesaBean.getIndirizzoResidenza(),apiIntesaBean.getNazioneResidenza(),apiIntesaBean.getLocalitaResidenza(),apiIntesaBean.getProvinciaResidenza()));
	baseRegistryData.setDomicile(apiIntesatoAddressData(apiIntesaBean.getIndirizzoDomicilio(),apiIntesaBean.getNazioneDomicilio(),apiIntesaBean.getLocalitaDomicilio(),apiIntesaBean.getProvinciaDomicilio()));
	
	
    ContactData contactData= new ContactData();
    if(null!=apiIntesaBean.getEmail()){
    	contactData.setEmail(apiIntesaBean.getEmail());
	 }
	
	if(null!=apiIntesaBean.getNumeroTelefono()){
		contactData.setNumber(apiIntesaBean.getNumeroTelefono());
	 }
	
    baseRegistryData.setContactData(contactData);
    
	 return baseRegistryData;
}

public BaseRegistryData apiIntesatoBaseRegistryDataAssicurato (ApiIntesa apiIntesaBean){
	
	 BaseRegistryData baseRegistryData = new BaseRegistryData();
	 if(null!=apiIntesaBean.getNomeAssicurato()){
		 baseRegistryData.setName(apiIntesaBean.getNomeAssicurato());
	 }
	 
	 if(null!=apiIntesaBean.getCognomeAssicurato()){
		 baseRegistryData.setSurname(apiIntesaBean.getCognomeAssicurato());
	 }
	 

	BirthData birthData= new BirthData();
	if(null!=apiIntesaBean.getDataNascitaAssicurato()){
		birthData.setBirthDate(df.format(apiIntesaBean.getDataNascitaAssicurato()));
	 }
	
	if(null!=apiIntesaBean.getCodiceFiscaleAssicurato()){
		birthData.setFiscalCode(apiIntesaBean.getCodiceFiscaleAssicurato());
	 }
	
	if(null!=apiIntesaBean.getLocalitaNascitaAssicurato()){
		birthData.setLocality(apiIntesaBean.getLocalitaNascitaAssicurato());
	 }
	
	if(null!=apiIntesaBean.getMicrochip()){
		birthData.setMicrochip(apiIntesaBean.getMicrochip());
	 }
	
	if(null!=apiIntesaBean.getNazioneNascitaAssicurato()){
		birthData.setNation(apiIntesaBean.getNazioneNascitaAssicurato());
	 }
	
	if(null!=apiIntesaBean.getProvinciaNascitaAssicurato()){
		birthData.setProvince(apiIntesaBean.getProvinciaNascitaAssicurato());
	 }
	
	
	baseRegistryData.setBirthData(birthData);
	
	baseRegistryData.setResidence(apiIntesatoAddressData(apiIntesaBean.getIndirizzoResidenzaAssicurato(),apiIntesaBean.getNazioneResidenzaAssicurato(),apiIntesaBean.getLocalitaResidenzaAssicurato(),apiIntesaBean.getProvinciaResidenzaAssicurato()));
	
    ContactData contactData= new ContactData();
    if(null!=apiIntesaBean.getEmailAssicurato()){
    	contactData.setEmail(apiIntesaBean.getEmailAssicurato());
	 }
	
	if(null!=apiIntesaBean.getNumeroTelefonoAssicurato()){
		contactData.setNumber(apiIntesaBean.getNumeroTelefonoAssicurato());
	 }
    
    baseRegistryData.setContactData(contactData);
   
	 return baseRegistryData;
}


public AddressData apiIntesatoAddressData(String address,String nation,String province,String place){
	  
	AddressData addressBean= new AddressData();
	if(null!=address){
		addressBean.setAddress(address);
	 } 
	
	if(null!=nation){
		addressBean.setNation(nation);
	 }
	
	if(null!=place){
		addressBean.setPlace(place);
	 }
	
	if(null!=province){
		addressBean.setProvince(province);
	 }
	
	
	  return addressBean;
}

  public ProductData apiIntesatoProductData(ApiIntesa apiIntesaBean){
	  
	ProductData productData= new ProductData();
	if(null!=apiIntesaBean.getCodiceProdotto()){
		productData.setCodeProduct(apiIntesaBean.getCodiceProdotto());
	 }
	
	if(null!=apiIntesaBean.getDescrizioneProdotto()){
		productData.setDescProduct(apiIntesaBean.getDescrizioneProdotto());
	 }
	
	if(null!=apiIntesaBean.getTipologiaProdotto()){
		productData.setProductType(apiIntesaBean.getTipologiaProdotto());
	 }
	
	
	  return productData;
  }
  
  public AdditionalData apiIntesatoAdditionalData(ApiIntesa apiIntesaBean){
	  
	  AdditionalData additionalData= new AdditionalData();
	  if(null!=apiIntesaBean.getCodiceClienteYOLO()){
		  additionalData.setClientCode(apiIntesaBean.getCodiceClienteYOLO());
		 }
	 
	  if(null!=apiIntesaBean.getDurata()){
		  additionalData.setCoverageDuration(apiIntesaBean.getDurata().toString());
		 }
	 
	  if(null!=apiIntesaBean.getDataInizioCopertura()){
		  additionalData.setCoverageStartDate(df.format(apiIntesaBean.getDataInizioCopertura()));
		 }
	 
	  if(null!=apiIntesaBean.getDataFineCopertura()){
		  additionalData.setCoverageEndDate(df.format(apiIntesaBean.getDataFineCopertura()));
		 }
	  
	  if(null!=apiIntesaBean.getStatoOrdine()){
		  additionalData.setOrderStatus(apiIntesaBean.getStatoOrdine());
		 }
	 
	  if(null!=apiIntesaBean.getNumeroPolizza()){
		  additionalData.setPolicyNumber(apiIntesaBean.getNumeroPolizza());
		 }
	 
	  if(null!=apiIntesaBean.getDataSottoscrizionePolizza()){
		  additionalData.setPolicySubscriptionDate(df.format(apiIntesaBean.getDataSottoscrizionePolizza()));
		 }
	  
	  if(null!=apiIntesaBean.getImportoPremio()){
		  additionalData.setPremiumAmount(apiIntesaBean.getImportoPremio().toString());
		 }
	  
	  if(null!=apiIntesaBean.getCanaleSottoscrizione()){
		  additionalData.setSubscriptionChannel(apiIntesaBean.getCanaleSottoscrizione());
		 }
	  
	 
	  return additionalData;
  }
}
